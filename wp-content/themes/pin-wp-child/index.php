<?php get_header(); // add header  ?>
<?php
// Options from admin panel
global $smof_data;

if (empty($smof_data['banner_p1'])) {
    $smof_data['banner_p1'] = '';
}
if (empty($smof_data['banner_p2'])) {
    $smof_data['banner_p2'] = '';
}
if (empty($smof_data['banner_p3'])) {
    $smof_data['banner_p3'] = '';
}
if (empty($smof_data['banner_p3'])) {
    $smof_data['banner_p4'] = '';
}
if (empty($smof_data['banner_p3'])) {
    $smof_data['banner_p5'] = '';
}
$home_pag_select = (isset($smof_data['home_pag_select'])) ? $smof_data['home_pag_select'] : 'Infinite Scroll';
$display_ads = (isset($smof_data['display_ads'])) ? $smof_data['display_ads'] : 'Yes';
?>

<!-- Begin Home Full width -->
<div class="home-fullwidth">

    <!-- Begin Sidebar (left) -->
    <?php get_template_part('sidebar2'); ?>
    <!-- end #sidebar (left) -->

    <!-- Begin Main Wrap Content -->
    <div class="wrap-content cf">
        <div class="wrap-content-inner">

            <?php if (is_home()): ?>
                <div class="shortcut cf">

                    <div class="shortcut-child">
                        <a href="http://heeladdict.com/boots/"><img
                                src="<?php bloginfo('stylesheet_directory'); ?>/images/home-cats-boots-w250.png"
                                alt="women"/>
                            <h3>Boots</h3></a>
                        <a href="http://heeladdict.com/frye/"><p class="shortcut-text">Frye</p></a>
                        <a href="http://heeladdict.com/rag-bone/"><p class="shortcut-text">Rag Bone</p></a>
                        <a href="http://heeladdict.com/calvin-klein/"><p class="shortcut-text">Calvin
                                Klein</p>
                        </a>
                    </div>

                    <div class="shortcut-child">
                        <a href="http://heeladdict.com/pumps/"><img
                                src="<?php bloginfo('stylesheet_directory'); ?>/images/home-cats-shoes-w250.png"
                                alt="pumps"/>
                            <h3>Pumps</h3></a>
                        <a href="http://heeladdict.com/jessica-simpson/"><p class="shortcut-text">Jessica
                                Simpson</p></a>
                        <a href="http://heeladdict.com/badgley-mischka/"><p class="shortcut-text">Badgley
                                Mischka</p></a>
                        <a href="http://heeladdict.com/rachel-zoe/"><p class="shortcut-text">Rachel Zoe</p>
                        </a>
                    </div>

                    <div class="shortcut-child">
                        <a href="http://heeladdict.com/ankle-boots/"><img
                                src="<?php bloginfo('stylesheet_directory'); ?>/images/home-cats--designers-w250.png"
                                alt="brands"/>
                            <h3>Brands</h3></a>
                        <a href="http://heeladdict.com/jimmy-choo/"><p class="shortcut-text">Jimmy Choo</p>
                        </a>
                        <a href="http://heeladdict.com/maison-margiela/"><p class="shortcut-text">Maison
                                Margiela</p></a>
                        <a href="http://heeladdict.com/nine-west/"><p class="shortcut-text">Nine West</p>
                        </a>
                    </div>

                </div>
            <?php endif; ?>
            <?php if (is_category()) { ?>
                <div class="archive-header"><h1><?php esc_html_e('Shop', 'anthemes'); ?>
                        <strong><?php single_cat_title(''); ?></strong></h1></div>
            <?php } elseif (is_tag()) { ?>
                <div class="archive-header"><h1><?php esc_html_e('All posts tagged in:', 'anthemes'); ?>
                        <strong><?php single_tag_title(''); ?></strong></h1></div>
            <?php } elseif (is_search()) { ?>
                <div class="archive-header">
                    <h1><?php printf(esc_html__('Search Results for: %s', 'anthemes'), '<strong>' . get_search_query() . '</strong>'); ?></h1>
                </div>
            <?php } elseif (is_author()) { ?>
                <div class="archive-header"><h1><?php esc_html_e('All posts by:', 'anthemes'); ?>
                        <strong><?php the_author(); ?></strong></h1></div>
            <?php } elseif (is_404()) { ?>
                <div class="archive-header"><h1><?php esc_html_e('Error 404 - Not Found', 'anthemes'); ?>
                        <br/> <?php esc_html_e('Sorry, but you are looking for something that isn\'t here.', 'anthemes'); ?>
                    </h1></div>
            <?php } elseif (is_home()) { ?>
                <h1 class="designer-clothes"> Shop designer clothes and footwear by top brands</h1>
            <?php } ?>

            <div class="archive-header">
                <?php include get_stylesheet_directory() . '/common/filter-frontend.php'; ?>
            </div>

            <ul id="infinite-articles" class="masonry_list js-masonry" data-masonry-options='{ "columnWidth": 0 }'>
                <?php $num = 0;
                if (have_posts()) : while (have_posts()) : the_post();
                    $num++; ?>

                    <?php if ($display_ads == 'Yes') { ?>
                        <?php if (!empty($smof_data['banner_300_1'])) { ?>
                            <?php if ($num == $smof_data['banner_p1']) {
                                echo '<li class="homeadv">' . stripslashes($smof_data['banner_300_1']) . ' <span> ' . esc_html__('Advertisement', 'anthemes') . '</span></li>';
                            } ?>
                        <?php } ?>
                        <?php if (!empty($smof_data['banner_300_2'])) { ?>
                            <?php if ($num == $smof_data['banner_p2']) {
                                echo '<li class="homeadv">' . stripslashes($smof_data['banner_300_2']) . ' <span> ' . esc_html__('Advertisement', 'anthemes') . '</span></li>';
                            } ?>
                        <?php } ?>
                        <?php if (!empty($smof_data['banner_300_3'])) { ?>
                            <?php if ($num == $smof_data['banner_p3']) {
                                echo '<li class="homeadv">' . stripslashes($smof_data['banner_300_3']) . ' <span> ' . esc_html__('Advertisement', 'anthemes') . '</span></li>';
                            } ?>
                        <?php } ?>
                        <?php if (!empty($smof_data['banner_300_4'])) { ?>
                            <?php if ($num == $smof_data['banner_p4']) {
                                echo '<li class="homeadv">' . stripslashes($smof_data['banner_300_4']) . ' <span> ' . esc_html__('Advertisement', 'anthemes') . '</span></li>';
                            } ?>
                        <?php } ?>
                        <?php if (!empty($smof_data['banner_300_5'])) { ?>
                            <?php if ($num == $smof_data['banner_p5']) {
                                echo '<li class="homeadv">' . stripslashes($smof_data['banner_300_5']) . ' <span> ' . esc_html__('Advertisement', 'anthemes') . '</span></li>';
                            } ?>
                        <?php } ?>
                    <?php } else { ?>
                        <?php if (!empty($smof_data['banner_300_1'])) { ?>
                            <?php if ($num == $smof_data['banner_p1']) {
                                echo '<li class="ex34 homeadv">' . stripslashes($smof_data['banner_300_1']) . ' <span> ' . esc_html__('Advertisement', 'anthemes') . '</span></li>';
                            } ?>
                        <?php } ?>
                        <?php if (!empty($smof_data['banner_300_2'])) { ?>
                            <?php if ($num == $smof_data['banner_p2']) {
                                echo '<li class="ex34 homeadv">' . stripslashes($smof_data['banner_300_2']) . ' <span> ' . esc_html__('Advertisement', 'anthemes') . '</span></li>';
                            } ?>
                        <?php } ?>
                        <?php if (!empty($smof_data['banner_300_3'])) { ?>
                            <?php if ($num == $smof_data['banner_p3']) {
                                echo '<li class="ex34 homeadv">' . stripslashes($smof_data['banner_300_3']) . ' <span> ' . esc_html__('Advertisement', 'anthemes') . '</span></li>';
                            } ?>
                        <?php } ?>
                        <?php if (!empty($smof_data['banner_300_4'])) { ?>
                            <?php if ($num == $smof_data['banner_p4']) {
                                echo '<li class="ex34 homeadv">' . stripslashes($smof_data['banner_300_4']) . ' <span> ' . esc_html__('Advertisement', 'anthemes') . '</span></li>';
                            } ?>
                        <?php } ?>
                        <?php if (!empty($smof_data['banner_300_5'])) { ?>
                            <?php if ($num == $smof_data['banner_p5']) {
                                echo '<li class="ex34 homeadv">' . stripslashes($smof_data['banner_300_5']) . ' <span> ' . esc_html__('Advertisement', 'anthemes') . '</span></li>';
                            } ?>
                        <?php } ?>
                    <?php } ?>

                    <li <?php post_class('ex34') ?> id="post-<?php the_ID(); ?>">

                        <?php if (has_post_thumbnail()) { ?>
                            <div
                                class="article-comm"><?php comments_popup_link('<i class="fa fa-comments"></i> 0', '<i class="fa fa-comments"></i> 1', '<i class="fa fa-comments"></i> %'); ?></div>
                            <div class="article-category"><i></i> <?php $category = get_the_category();
                                if ($category) {
                                    echo wp_kses_post('<a href="' . get_category_link($category[0]->term_id) . '" class="tiptipBlog" title="' . sprintf(esc_html__("View all posts in %s", "anthemes"), $category[0]->name) . '" ' . '>' . $category[0]->name . '</a> ');
                                } ?>
                            </div><!-- end .article-category -->
                            <a href="<?php the_permalink(); ?>"> <?php echo the_post_thumbnail('thumbnail-blog-masonry'); ?> </a>

                            <div class="home-share">
                                <?php $video_wp_facebooklink = 'https://www.facebook.com/sharer/sharer.php?u='; ?>
                                <a class="fbbutton" target="_blank"
                                   href="<?php echo esc_url($video_wp_facebooklink); ?><?php the_permalink(); ?>&amp;=<?php the_title(); ?>"><i
                                        class="fa fa-facebook"></i></a>
                                <?php $video_wp_twitterlink = 'https://twitter.com/home?status=Check%20out%20this%20article:%20'; ?>
                                <a class="twbutton" target="_blank"
                                   href="<?php echo esc_url($video_wp_twitterlink); ?><?php the_title(); ?>%20-%20<?php the_permalink(); ?>"><i
                                        class="fa fa-twitter"></i></a>
                                <?php $articleimage = wp_get_attachment_url(get_post_thumbnail_id($post->ID)); ?>
                                <?php $video_wp_pinlink = 'https://pinterest.com/pin/create/button/?url='; ?>
                                <a class="pinbutton" target="_blank"
                                   href="<?php echo esc_url($video_wp_pinlink); ?><?php the_permalink(); ?>&amp;media=<?php echo esc_html($articleimage); ?>&amp;description=<?php the_title(); ?>"><i
                                        class="fa fa-pinterest"></i></a>
                                <?php $video_wp_googlelink = 'https://plus.google.com/share?url='; ?>
                                <a class="googlebutton" target="_blank"
                                   href="<?php echo esc_url($video_wp_googlelink); ?><?php the_permalink(); ?>"><i
                                        class="fa fa-google-plus-square"></i></a>
                                <?php $video_wp_emaillink = 'mailto:?subject='; ?>
                                <a class="emailbutton" target="_blank"
                                   href="<?php echo esc_url($video_wp_emaillink); ?><?php the_title(); ?>&amp;body=<?php the_permalink(); ?> <?php echo anthemes_excerpt(strip_tags(strip_shortcodes(get_the_excerpt())), 140); ?>"><i
                                        class="fa fa-envelope"></i></a>
                            </div><!-- end #home-share -->

                        <?php } else { ?>

                            <a href="<?php echo rwmb_meta('_buyUrl') ?>" class="product-img-link" rel="nofollow" target="_blank">
                                <img class="attachment-thumbnail-blog-masonry wp-post-image "
                                     onerror="this.onerror=null;this.src='<?php echo get_stylesheet_directory_uri() . '/images/image-coming-soon.png' ?>';"
                                     src="<?php echo rwmb_meta('_image') ?>"
                                />
                            </a>

                        <?php } // Post Thumbnail ?>

                        <div class="clear"></div>

                        <div class="small-content">
                            <div class="an-widget-title">
                                <h2 class="article-title entry-title">
                                    <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                                <?php if (function_exists('taqyeem_get_score')) { ?>
                                    <?php taqyeem_get_score(); ?>
                                <?php } ?>
                            </div>
                            <p class="price">
                                <?php if (rwmb_meta('_salePrice')) { ?>
                                    <span class="old-price">
                            <?php st_the_price() ?>
                        </span>
                                    <span class="sale-price">
                            <?php st_the_price(true) ?>
                        </span>
                                <?php } else { ?>
                                    <span class="regular-price">
                            <?php st_the_price() ?>
                        </span>
                                <?php } ?>
                            </p>
                            <p><?php echo anthemes_excerpt(strip_tags(strip_shortcodes(get_the_excerpt())), 80); ?></p>
                        </div><!-- end .small-content -->
                        <a href="<?php echo rwmb_meta('_buyUrl') ?>" target="_blank" class="btn-st" rel="nofollow">Take me home <span class="arrow">></span> </a>
                        
                    </li>
                <?php endwhile; ?>
                <?php else: ?>
                    <p class="no-results">
                        Sorry, we could not find a match for your search - please
                        <a href="<?php echo site_url() ?>">start again</a>
                    </p>

                <?php endif; ?>
            </ul><!-- end .masonry_list -->

            <!-- Pagination -->
            <?php if ($home_pag_select == 'Infinite Scroll') { ?>
                <div id="nav-below" class="pagination" style="display: none;">
                    <div
                        class="nav-previous"><?php previous_posts_link('&lsaquo; ' . esc_html__('Newer Entries', 'anthemes') . ''); ?></div>
                    <div
                        class="nav-next"><?php next_posts_link('' . esc_html__('Older Entries', 'anthemes') . ' &rsaquo;'); ?></div>
                </div>
            <?php } else { // Infinite Scroll ?>
                <div class="clear"></div>
                <?php if (function_exists('wp_pagenavi')) { ?>
                    <?php wp_pagenavi(); ?>
                <?php } else { ?>
                    <div class="defaultpag">
                        <div
                            class="sright"><?php next_posts_link('' . esc_html__('Older Entries', 'anthemes') . ' &rsaquo;'); ?></div>
                        <div
                            class="sleft"><?php previous_posts_link('&lsaquo; ' . esc_html__('Newer Entries', 'anthemes') . ''); ?></div>
                    </div>
                <?php } ?>
            <?php } // Default Pagination ?>
            <!-- pagination -->
        </div>
    </div><!-- end .wrap-content -->


    <div class="clear"></div>
</div><!-- end .home-fullwidth -->
<?php get_footer(); // add footer  ?>
